import '../index.html';
import '../style.css';
import '../../app.config';
import 'mapbox-gl/dist/mapbox-gl.css';
import mapper from './Mapbox';

class App {

    domNewTitle;
    domNewContent;
    domNewTimeStart;
    domNewTimeEnd;
    domNewLong;
    domNewLatt;
    domBtnNewAdd;
    domBtnUpdate;

    start() {

        console.log( 'Application started' );

        this.initHtml();

        this.domBtnNewAdd.addEventListener( 'click', mapper.MarkerCreateAdd.bind(mapper) );

        this.domBtnUpdate.addEventListener('click', mapper.RefreshMarker.bind(mapper) );
    }

    
    initHtml(){
        this.domNewTitle = document.querySelector( '#eventTitle' );
        this.domNewTitle.addEventListener( 'focus', this.onErrorRemove.bind(this));
        
        this.domNewContent = document.querySelector( '#eventContent' );
        this.domNewContent.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewTimeStart = document.querySelector( '#eventTimeStart' );
    this.domNewTimeStart.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewTimeEnd = document.querySelector( '#eventTimeEnd' );
        this.domNewTimeEnd.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domNewLong = document.querySelector( '#eventCoordsLong' );
        this.domNewLong.addEventListener( 'focus', this.onErrorRemove.bind(this));


        this.domNewLatt = document.querySelector( '#eventCoordsLatt' );
        this.domNewLatt.addEventListener( 'focus', this.onErrorRemove.bind(this));

        this.domBtnNewAdd = document.querySelector( '#eventAdd' );

        this.domBtnUpdate = document.querySelector( '#update');
    }

    onErrorRemove( evt ) {
        evt.target.classList.remove( 'error' );
    }


}



const instance = new App();

export default instance;