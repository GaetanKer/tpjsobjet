import appConfig from '../../app.config';
import 'mapbox-gl/dist/mapbox-gl.css';
import instance from './App';
import { LocalEvent } from './LocalEvent';

const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

class Mapbox {

    map;
    EventList = [];

    constructor() {

        console.log('map lancé');

        mapboxgl.accessToken = appConfig.mapboxKey;
        this.map = new mapboxgl.Map({
            container: 'mapbox',
            center: { lng: 2.7885292, lat: 42.6832109 },
            zoom: 14,
            style: 'mapbox://styles/mapbox/dark-v10'
        });

        // Ajout d'un écouteur de clic sur la map (fonctionne pareil que addEventListener())
        this.map.on( 'click', function(evt){ 

            instance.domNewLong.value = evt.lngLat.lng;
            instance.domNewLatt.value = evt.lngLat.lat; 
            
        });

        this.RefreshMarker();

    }



    createMarkerMap(NewLocalEvent){



        const date = Date.now();

        let color;

        let msg;

        const time = NewLocalEvent.timeStart.getTime() - date;

        if ( (time) > ( 1000 * 60 * 60 * 24 * 3 ) ) {
            color = "#1df800ad"; 
            msg= "";
        }

        else if ( (time) < ( 1000 * 60 * 60 * 24 * 3 ) && (time) > 0 ){

            color = "#c24e00";
            let day = parseInt(time/(1000 * 60 * 60 * 24));

            let hours = parseInt( ( time - ( day * 1000 * 60 * 60 * 24 ) ) / ( 1000 * 60 * 60 ) );
            
            msg= "Attention, commence dans " + day + " jours et " + hours + " heures.";

        }
        else{
            color = "#ff0000";
            msg= "Quel Dommage! Vous avez raté cet évenment!";
        }

        const marker = new mapboxgl.Marker({ color: color });

        marker.getElement().title = NewLocalEvent.title;

        const popup = new mapboxgl.Popup();

        popup.setHTML('<span class = popupTitle>' + NewLocalEvent.title + '</span><br>' + NewLocalEvent.content + '<br> Début de l\'évenement : <br>' + NewLocalEvent.timeStart.toLocaleString() + '<br> Fin de l\'évenement : <br>' + NewLocalEvent.timeEnd.toLocaleString() + "<br>" + msg );

        marker.setPopup( popup );

        marker.setLngLat({ lng: NewLocalEvent.long, lat: NewLocalEvent.latt });

        marker.addTo( this.map );

    }


    loadEvent() {
        const storageContent = localStorage.getItem( appConfig.localStorageName );
        if( storageContent === null ) {
            return;
        }

        let storedJson;

        try {
            storedJson = JSON.parse( storageContent );
        }
        // Si le contenu est corrompu, on le supprime et on arrête de traitement
        catch( error ) {
            localStorage.removeItem( appConfig.localStorageName );
            return;
        }

        // On crée les events à partir du JSON obtenu
        for( let jsonEvent of storedJson ) {
            const localEvent = new LocalEvent(jsonEvent);
            this.EventList.push( localEvent );
        }
    }

    UpdateMarker() {
        // 1-Effacer tout les marqueurs de la map

        let Markers = document.querySelectorAll('.mapboxgl-marker');

        for (let marker of Markers){
            marker.remove();
        }

        // TODO: coder la fonction



        // 2- Reconstruction à partir de la liste en mémoire
        for( let event of this.EventList ) {



            event.timeStart = new Date(event.timeStart);

            event.timeEnd = new Date(event.timeEnd);
            
            this.createMarkerMap( event );

        }
    }

    RefreshMarker(){

        this.loadEvent();

        this.UpdateMarker();
    }

    MarkerCreateAdd(){

        let hasError = false;

        if(instance.domNewTitle.value === ''){
            instance.domNewTitle.classList.add( 'error' );
            instance.domNewTitle.value;
            hasError = true;
        }


        if(instance.domNewContent.value === ''){
            instance.domNewContent.classList.add( 'error' );
            instance.domNewContent.value;
            hasError = true;
        }
        if(instance.domNewTimeStart.value === ''){
            instance.domNewTimeStart.classList.add( 'error' );
            instance.domNewTimeStart.value;
            hasError = true;
        }
        if(instance.domNewContent.value === ''){
            instance.domNewTimeEnd.classList.add( 'error' );
            instance.domNewTimeEnd.value;
            hasError = true;
        }


        if(instance.domNewLong.value === ''){
            instance.domNewLong.classList.add( 'error' );
            instance.domNewLong.value;
            hasError = true;
        }

        if(instance.domNewLatt.value === ''){
            instance.domNewLatt.classList.add( 'error' );
            instance.domNewLatt.value;
            hasError = true;
        }

        if (hasError === true){
            return;
        }

        

        const newJson = {
            
            title: instance.domNewTitle.value,
            content: instance.domNewContent.value,
            timeStart: new Date(instance.domNewTimeStart.value),
            timeEnd: new Date(instance.domNewTimeEnd.value),
            long: instance.domNewLong.value,
            latt: instance.domNewLatt.value,
        };

        instance.domNewTitle.value = instance.domNewContent.value =  "";

        const newLocalEvent = new LocalEvent(newJson);

        this.createMarkerMap( newLocalEvent );

        this.EventList.push( newLocalEvent );
       
        localStorage.setItem( appConfig.localStorageName, JSON.stringify( this.EventList ) );

    }


    
    
}

const mapper = new Mapbox();
export default mapper;