
export class LocalEvent {

    title;
    content;
    timeStart;
    timeEnd;
    long;
    latt;

    constructor( jsonEvent ) {

        this.title = jsonEvent.title;
        this.content = jsonEvent.content;
        this.timeStart = jsonEvent.timeStart;
        this.timeEnd = jsonEvent.timeEnd;
        this.long = jsonEvent.long;
        this.latt = jsonEvent.latt;

    }

    

    toJSON() {

        return {

            title: this.title,
            content: this.content,
            timeStart: this.timeStart,
            timeEnd: this.timeEnd,
            long: this.long,
            latt: this.latt

        }

    }

}